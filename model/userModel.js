const mongoose = require("mongoose")


const userSchema=mongoose.Schema({
    name: {
        type: String,
        require:true,
    },
    birthday: {
        type: String,
        require: true,
    },
    gender: {
        type: String,
        require: true
    },
    phone: {
        type: String,
        require: true,
    },
    email: {
        type: String,
        require: true
    },
    password: {
        type:String,
        require: true,
    },
    address: {
        type: String,
    },
    isAdmin: {
        type:Boolean,
        default:false
    }
})


let User = mongoose.model("User",userSchema)



module.exports = { User }