const mongoose = require("mongoose")
const scheduleSchema=mongoose.Schema({
    room: {
        type: String,
        require: true
    },
    seat: {
        "8:00":{
            name: {
                type:String,
                default:null
            },
            seat: {
                type: Array,
                default: []
            }
        },
        "10:00":{
            name: {
                type:String,
                default:null
            },
            seat: {
                type: Array,
                default: []
            }
        },
        "12:00":{
            name: {
                type:String,
                default:null
            },
            seat: {
                type: Array,
                default: []
            }
        },
        "14:00":{
            name: {
                type:String,
                default:null
            },
            seat: {
                type: Array,
                default: []
            }
        },
        "16:00":{
            name: {
                type:String,
                default:null
            },
            seat: {
                type: Array,
                default: []
            }
        },
        "18:00":{
            name: {
                type:String,
                default:null
            },
            seat: {
                type: Array,
                default: []
            }
        },
        "20:00":{
            name: {
                type:String,
                default:null
            },
            seat: {
                type: Array,
                default: []
            }
        },
        "22:00":{
            name: {
                type:String,
                default:null
            },
            seat: {
                type: Array,
                default: []
            }
        },
        "24:00":{
            name: {
                type:String,
                default:null
            },
            seat: {
                type: Array,
                default: []
            }
        },
    }
})

let Schedule = mongoose.model("Schedule",scheduleSchema)
module.exports = { Schedule }