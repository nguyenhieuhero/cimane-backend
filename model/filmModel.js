const mongoose = require("mongoose")
const filmSchema=mongoose.Schema({
    name: {
        type:String,
        require:true
    },
    engName:{
        type:String,
        require:true
    },
    director: {
        type:String
    },
    actor: {
        type: String
    },
    type: {
        type: String
    },
    duration: {
        type: String
    },
    lang:{
        type: String
    },
    description: {
        type: String
    },
    url: {
        trailerUrl: {
            type:String,
        },
        imgUrl: {
            type:String,
        },
        slideImgUrl: {
            type:String
        },
    },
    rooms: {
        type: Array,
        require: true
    }
})
let Film = mongoose.model("Film",filmSchema)
module.exports={Film}