const { Film } = require("../model/filmModel")
const { Schedule } = require("../model/scheduleModel")

const filmController = {
    addFilm: async (req,res) => {
        try{
            const film = new Film(req.body)
            const savedFilm = await film.save()
            console.log(savedFilm)
            res.status(200).json(savedFilm)
        }catch(error){
            res.status(500).json(error)
        }
    },
    getAllFilm: async (req,res) => {
        try {
            const films= await Film.find()
            res.status(200).json(films)
        } catch (error) {
            res.status(500).json(error)
        }
    },
    getFilm: async (req,res) => {
        try {
            const film = await Film.find({engName:req.params.engName})
            console.log(req.params.engName)
            res.status(200).json(film)
        } catch (error) {
            res.status(500).json(error)
        }
    },
    updateFilm :async (req,res) => {
        try {
            const film = await Film.find({engName:req.body.name})
            await film.updateOne({$set:{"rooms.0":null}})
            res.status(200).json(film)
        } catch (error) {
            res.status(500).json(error)
        }
    },
};

module.exports = filmController;