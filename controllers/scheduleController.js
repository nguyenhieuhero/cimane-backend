const { Schedule } = require("../model/scheduleModel")
const { Film } = require("../model/filmModel")

const scheduleController = {
    addSchedule: async (req,res) => {
        try{
            const exist=await Schedule.findOne({room:req.body.room})
            if(exist){
                res.status(200).json("Lich da ton tai")
            }
            else{
                const schedule = new Schedule(req.body)
                const savedSchedule = await schedule.save()
                res.status(200).json(savedSchedule)
            }
        }catch(error){
            res.status(500).json(error)
        }
    },
    getOneSchedule: async (req,res) => {
        try {
            const schedule = await Schedule.find({room:req.params.room})
            res.status(200).json(schedule)
        } catch (error) {
            res.status(500).json(error)
        }
    },
    getAllSchedule: async (req,res) => {
        try {
            const schedules= await Schedule.find()
            res.status(200).json(schedules)
        } catch (error) {
            res.status(500).json(error)
        }
    },
    updateSchedule: async (req,res) => {
        try {
            const schedule = await Schedule.findOneAndUpdate({room:req.body.room},{$set:{[`seat.${req.body.time}.name`]:req.body.name}})
            const film = await Film.findOneAndUpdate({engName:req.body.name},{$push:{rooms:{[req.body.room]:req.body.time}}})
            res.status(200).json(schedule)
        } catch (error) {
            res.status(500).json(error)
        }
    },
    booking: async (req,res) => {
        try {
            const exist=await Schedule.findOne({[`seat.${req.body.time}.seat`]:{$in:req.body.seat}})
            if(exist){
                res.status(200).json({message:"da ton tai",success:false})
            }
            else{
                const schedule = await Schedule.findOneAndUpdate({room:req.body.room},{$push:{[`seat.${req.body.time}.seat`]:{$each:req.body.seat}}})
                res.status(200).json({message:"thanh cong",success:true})    
            }
        } catch (error) {
            res.status(500).json(error)
        }
    },
};

module.exports = scheduleController;