const router = require("express").Router()
const scheduleController = require("../controllers/scheduleController")


router.post("/",scheduleController.addSchedule)
router.get("/",scheduleController.getAllSchedule)
router.post("/update",scheduleController.updateSchedule)
router.get("/:room",scheduleController.getOneSchedule)
router.post("/booking",scheduleController.booking)
module.exports = router;