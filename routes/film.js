const router = require("express").Router()
const filmController = require("../controllers/filmController")


router.post("/",filmController.addFilm)
router.get("/",filmController.getAllFilm)
router.get("/:engName",filmController.getFilm)
router.post("/update",filmController.updateFilm)
module.exports = router;