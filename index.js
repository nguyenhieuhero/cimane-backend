const express = require("express")
const cors = require("cors")
const app = express()
const mongoose = require("mongoose")
var bodyParser = require("body-parser")
const morgan = require("morgan")
const dotenv = require("dotenv")
const filmRoute = require("./routes/film")
const authRoute = require("./routes/user")
const scheduleRoute = require("./routes/schedule")



app.use(bodyParser.json({limit:"50mb"}))
app.use(cors())
app.use(morgan("common"))

dotenv.config()

app.use("/film",filmRoute)
app.use("/login",authRoute)
app.use("/schedule",scheduleRoute)
app.listen(8000,()=>console.log("Sever is running..."))
mongoose.connect((process.env.MONGODB_URL), ()=>
    console.log("Connected to MongoDB")
)